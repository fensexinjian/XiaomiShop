import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//渲染md 文件样式
import 'github-markdown-css';
// ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 公共样式
import './assets/common.css'
import './assets/reser.css'
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
