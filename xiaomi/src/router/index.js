import Vue from 'vue'
import VueRouter from 'vue-router'
import FirstPage from '../views/FirstPage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'FirstPage',
    component: FirstPage
  },
  {
    path: '/AllGoods',
    name: 'AllGoods',
    component: () => import( '../views/AllGoods.vue')
  },
  {
    path: '/AboutUs',
    name: 'AboutUs',
    component: () => import( '../views/AboutUs.vue')
  },
  {
    path: '/details',
    name: 'details',
    component: () => import( '../views/details.vue')
  },
  {
    path: '/shopCar',
    name: 'shopCar',
    component: () => import( '../views/shopCar.vue')
  },
  {
    path: '/myCollection',
    name: 'myCollection',
    component: () => import( '../views/MY/myCollection.vue')
  },
  {
    path: '/myOrder',
    name: 'myOrder',
    component: () => import( '../views/MY/myOrder.vue')
  },
  {
    path: '/confirmOrder',
    name: 'confirmOrder',
    component: () => import( '../views/confirmOrder.vue')
  }
]
const router = new VueRouter({
  routes
})

export default router
