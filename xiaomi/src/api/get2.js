import request from './request.js'
// 注册
 async function getRegister(params) {
    return await request({
        url:"users/findUserName",
        method:"post",
        data:params
    })
}
// 登录
 async function getlogin(params) {
    return await request({
        url:"users/login",
        method:"post",
        data:params
    })
}
// 加入购物车
 async function getAddShoppingCart(params) {
    return await request({
        url:"user/shoppingCart/addShoppingCart",
        method:"post",
        data:params
    })
}
// 加入收藏
 async function getaddCollect(params) {
    return await request({
        url:"user/collect/addCollect",
        method:"post",
        data:params
    })
}
// 获取收藏
 async function getCollect(params) {
    return await request({
        url:"user/collect/getCollect",
        method:"post",
        data:params
    })
}
// 删除收藏
 async function deleteCollect(params) {
    return await request({
        url:"user/collect/deleteCollect",
        method:"post",
        data:params
    })
}

// 购物车
 async function getShoppingCart(params) {
     console.log(params);
    return await request({
        url:"user/shoppingCart/getShoppingCart",
        method:"post",
        data:params
    })
}
//删除
async function deleteShoppingCart(params) {
    return await request({
        url:"user/shoppingCart/deleteShoppingCart",
        method:"post",
        data:params
    })
}
//步进器
async function updateShoppingCart(params) {
    return await request({
        url:"user/shoppingCart/updateShoppingCart",
        method:"post",
        data:params
    })
}
//添加结算
async function addOrder(params) {
    console.log(params);
    return await request({
        url:"user/order/addOrder",
        method:"post",
        data:params
    })
}
//得到订单
async function getOrder(params) {
    console.log(params);
    return await request({
        url:"user/order/getOrder",
        method:"post",
        data:params
    })
}


export { getRegister,getlogin,getAddShoppingCart,getShoppingCart,getaddCollect,getCollect,deleteCollect,deleteShoppingCart,updateShoppingCart,addOrder,getOrder}