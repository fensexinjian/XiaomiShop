import request from './request.js'
// 首页
 async function getHeader() {
    return await request({
        url:"resources/carousel",
        method:"post",
    })
}

 async function getPromoProduct(parmas) {
    return await request({
        url:"product/getPromoProduct",
        method:"post",
        data:parmas
    })
}
async function getHotProduct(parmas) {
    return await request({
        url:"product/getHotProduct",
        method:"post",
        data:parmas
    })
}
// 分类页

//获取分类数据
async function getProductBySearch(parmas) {
    return await request({
        url:"product/getProductBySearch",
        method:"post",
        data:parmas
    })
}
//获取分类数据
async function getCategory(parmas) {
    return await request({
        url:"product/getCategory",
        method:"post",
        data:parmas
    })
}
//获取全部商品
async function getAllProduct(parmas) {
    return await request({
        url:"product/getAllProduct",
        method:"post",
        data:parmas
    })
}
//商品分类类型数据
async function getProductByCategory(parmas) {
    return await request({
        url:"product/getProductByCategory",
        method:"post",
        data:parmas
    })
}

// 关于我们
async function getREADME() {
    return await request({
        url:"public/docs/README.md",
        method:"get"
    })
}
//详情
async function getDetails(params) {
    // console.log(params.productID);
    return await request({
        url:"product/getDetails",
        method:"POST",
        data:params
    })
}
//详情轮播
async function getDetailsPicture(params) {
    // console.log(params.productID);
    return await request({
        url:"product/getDetailsPicture",
        method:"POST",
        data:params
    })
}

export {getHeader,getPromoProduct,getHotProduct,getCategory,getProductBySearch,getAllProduct,getProductByCategory,getREADME,getDetails,getDetailsPicture}