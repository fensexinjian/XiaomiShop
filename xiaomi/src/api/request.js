import Axios from "axios";

// 1.创建axios的实例
const instance = Axios.create({
//本地json
  baseURL: "/api",
//开发环境跨域-npm run dev ,需要在vue.config.js里配置代理跨域proxy
//       baseURL:"/api",
//线上环境跨域-npm run build。可用第三方代理或者让后台给你开跨域
  // baseURL:"https://bird.ioliu.cn/v1?url=http://ustbhuangyi.com/",
  timeout: 5000,
  //可统一配置其它属性
  // responseType: "json",
  // withCredentials: true, // 是否允许带cookie这些
  // headers: {
  //     "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
  // },
});

// 配置请求拦截器-常见的就是统一加入请求头
instance.interceptors.request.use(
  config => {
  // 在发送请求之前做某件事-若是有做鉴权token , 就给头部带上token
  // if (localStorage.token) {
  //     config.headers.Authorization = localStorage.token;
  // }
  //  console.log('发送中',config); //config就是axios里返回的promise对象
    return config;
  },
  err => {
    // console.log('发送失败');
    return err;
  }
);

  //配置响应拦截器——对响应数据做些事,常见的就是设置一些统一错误弹窗
instance.interceptors.response.use(
  response => {
      //  console.log('响应成功',response);
    return response.data;
  },
  err => {
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = "请求错误";
          break;
        case 401:
          err.message = "未授权的访问";
          break;
      }
    }
      // console.log('响应失败1');
    return err;
    
  }
);

export default  instance


