import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    user:{},
    shopCarData: [], //购物车数据
    result: [], //购物车选中数据
  },
  mutations: {
    getDate(state) {
      var str = localStorage.getItem("token") || "[]"
      state.user=JSON.parse(str)
    },
    // 添加购物车
    addShopCar(state,params){
      state.shopCarData.push(params)
      console.log("18state.shopCarData",state.shopCarData);
    },
    getShopCarData(state, params) { //获取购物车数据
      state.shopCarData = params
      // console.log(14, state.shopCarData);
    },
    getShopCarResult(state, params) { //获取购物车选中数据
      state.result = params
      // console.log(14, state.result);
    }
  },
  getters: {
    // 求购物车商品总数
    allNumber(state) {
      let a = 0
      state.shopCarData.forEach(element => {
        console.log("element.num",element.num);
        a += element.num
        console.log(a);
      });
      return a
    },
    // 求购物车已选择商品总价
    selectPrice(state) {
      let a = 0
      state.result.forEach(element => {
        a += element.num * element.price
      });
      return a
    },
    //购物车已选择商品数量
    selectNumber(state) {
      let a = 0
      state.result.forEach(element => {
        a += element.num
      });
      return a
    }
  },
  actions: {
  async setShoppingCart ({ commit }, data) {
    //  console.log(data);
     await commit('getShopCarData', data);
    },
  },
  modules: {}
})